/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mazmellow.chillingsong01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import buzzcity.android.sdk.BCAdsClientBanner;

import com.mazmellow.chillingsong01.RequestHttpClient.RequestHttpClientListenner;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class ImageListActivity extends AbsListViewBaseActivity implements
		RequestHttpClientListenner {

	DisplayImageOptions options;

	String[] titles;
	String[] imageUrls;
	String[] descs;
	String[] vCodes;

	boolean isLoadComplete;

	ItemAdapter adapter;

	// ----
	RequestHttpClient httpClient;
	String url_user = /*"http://thaigirlsexy.blogspot.com/2013/11/chillingsong.html";/*/"http://www.mazmellow.com/chilling_song.php";
	public static DatabaseManager myDb;
	public static ArrayList<String> bookmarkList;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_list);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		BaseActivity.imageLoader.init(ImageLoaderConfiguration
				.createDefault(this));

		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
				.cacheOnDisc(true)
				.displayer(new FadeInBitmapDisplayer(500)/*
														 * RoundedBitmapDisplayer(
														 * 20)
														 */).build();

		listView = (ListView) findViewById(android.R.id.list);
		adapter = new ItemAdapter();
		((ListView) listView).setAdapter(adapter);

		myDb = new DatabaseManager(this);

		if (!isLoadComplete) {
			// request
			httpClient = new RequestHttpClient(url_user, this, this);
			httpClient.start();
		} else {
			refreshBookmark();
		}

//		BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(106400,
//				BCAdsClientBanner.ADTYPE_MWEB,
//				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
//		ImageView graphicalAds = (ImageView) findViewById(R.id.ads0);
//		graphicAdClient.getGraphicalAd(graphicalAds);

		BCAdsClientBanner graphicAdClient2 = new BCAdsClientBanner(101021,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds2 = (ImageView) findViewById(R.id.ads1);
		graphicAdClient2.getGraphicalAd(graphicalAds2);

	}
	
	public void onClickAppList(View view){
		Intent intent = new Intent(this, AppListActivity.class);
		startActivity(intent);
	}

	public void clickList(int position) {
		// TODO Auto-generated method stub

		DetailActivity.title = titles[position];
		DetailActivity.desc = descs[position];
		DetailActivity.img = imageUrls[position];
		DetailActivity.vcode = vCodes[position];

		Intent intent = new Intent(this, DetailActivity.class);
		startActivity(intent);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		refreshBookmark();
	}

	public void refreshBookmark() {
		if (myDb != null) {
			bookmarkList = myDb.getAllBookmark();
			if (bookmarkList == null) {
				bookmarkList = new ArrayList<String>();
			}
		}

		// refresh list
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}

	class ItemAdapter extends BaseAdapter {

		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

		private class ViewHolder {
			public TextView text;
			public ImageView image;
			public Button starBtn;
		}

		public int getCount() {
			if (imageUrls != null) {
				return imageUrls.length;
			}
			return 0;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.youtube_item,
						parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				holder.image = (ImageView) view.findViewById(R.id.image);
				holder.starBtn = (Button) view.findViewById(R.id.starBtn);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			holder.text.setText(titles[position]);
			if (bookmarkList.indexOf(vCodes[position]) != -1) {
				holder.starBtn.setBackgroundResource(R.drawable.icon_star);
			} else {
				holder.starBtn.setBackgroundResource(R.drawable.icon_star_gray);
			}

			holder.starBtn.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!myDb.checkHasBookmarkByVcode(vCodes[position])) {
						// bookmark
						myDb.insertBookmark(vCodes[position]);
					} else {
						myDb.deleteBookmarkByVcode(vCodes[position]);
					}
					refreshBookmark();
				}
			});

			imageLoader.displayImage(imageUrls[position], holder.image,
					options, animateFirstListener);

			view.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					clickList(position);
				}
			});

			return view;
		}
	}

	public void onRequestStringCallback(String response) {
		// TODO Auto-generated method stub

//		String startIndex = "startjson";
//		String endIndex = "endjson";
//		response = response.substring(
//				response.indexOf(startIndex) + startIndex.length(),
//				response.indexOf(endIndex));
		
		try {
			JSONArray jsonArray = new JSONArray(response);

			if (jsonArray != null && jsonArray.length() > 0) {

				titles = new String[jsonArray.length()];
				descs = new String[jsonArray.length()];
				imageUrls = new String[jsonArray.length()];
				vCodes = new String[jsonArray.length()];

				for (int i = jsonArray.length() - 1; i >= 0; i--) {
					JSONObject jsonObj = jsonArray.getJSONObject(i);

					if (jsonObj.has("title"))
						titles[i] = jsonObj.getString("title");
					if (jsonObj.has("desc"))
						descs[i] = jsonObj.getString("desc");
					if (jsonObj.has("img"))
						imageUrls[i] = jsonObj.getString("img");
					if (jsonObj.has("vcode"))
						vCodes[i] = jsonObj.getString("vcode");

				}
				isLoadComplete = true;
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error = " + e.getMessage());
		}

		refreshBookmark();
	}

	public void onBackPressed() {
		AnimateFirstDisplayListener.displayedImages.clear();
		super.onBackPressed();
	}

	public static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

}