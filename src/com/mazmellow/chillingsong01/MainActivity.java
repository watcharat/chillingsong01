package com.mazmellow.chillingsong01;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.mazmellow.chillingsong01.RequestHttpClient.RequestHttpClientListenner;

public class MainActivity extends Activity implements
		RequestHttpClientListenner {

	boolean request_playlist = false;
	boolean request_user = false;

	RequestHttpClient httpClient;
	// String url_playlist =
	// "http://gdata.youtube.com/feeds/api/playlists/RD021kz6hNDlEEg?v=1&alt=json";

	// onefotoable ok
	// awaziavimotihca
	// String url_user =
	// "http://gdata.youtube.com/feeds/api/users/awaziavimotihca/uploads?&v=2&max-results=50&alt=jsonc";
	// //UCHmpi5o1Fm2PDGa1izasg8w WishesOnTheEarth

	// chilling_song
	// "http://maz-mazmellow.blogspot.com/2013/11/titlemovie-1urlhttp119.html"
	String url_user = "http://mazmelllow.zz.mu/chilling_song.php";

	ArrayList<YoutubeObject> array_playlist;
	ArrayList<YoutubeObject> array_user;

	double longitude;
	double latitude;

	public static String countryCode;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	}

	private void checkLocation() {
		// check if GPS enabled
		GPSTracker gpsTracker = new GPSTracker(this);

		if (gpsTracker.canGetLocation()) {
			String stringLatitude = String.valueOf(gpsTracker.latitude);
			System.out.println("stringLatitude = " + stringLatitude);

			String stringLongitude = String.valueOf(gpsTracker.longitude);
			System.out.println("stringLongitude = " + stringLongitude);

			// doRequestCountry(stringLatitude,stringLongitude);
		} else {
			gpsTracker.showSettingsAlert();
		}
	}

	// private boolean isActivityFinished = false;
	//
	//
	// public void onPause() {
	// super.onPause();
	// if(isActivityFinished) {
	// Intent i = new Intent(getApplicationContext(), AdsWrapper.class);
	// i.putExtra("partnerId", "101021");
	// i.putExtra("appId", "com.example.testyoutube");
	// i.putExtra("showAt", "end");
	// i.putExtra("skipEarly", "true");
	// i.putExtra("adsTimeout", "10");
	// startActivity(i);
	// }
	// }
	//
	//
	// public void finish() {
	// isActivityFinished = true;
	// super.finish();
	// }

	private void doRequestCountry(String lat, String lon) {
		String url = "http://api.worldweatheronline.com/free/v1/search.ashx?q="
				+ lat + "," + lon + "&format=json&key=tc4feefhdfuh75f6pvupfnhz";
		RequestHttpClient locationClient = new RequestHttpClient(url,
				new RequestHttpClientListenner() {

					public void onRequestStringCallback(String response) {
						// TODO Auto-generated method stub
						try {
							JSONObject jsonObject = new JSONObject(response);
							JSONObject search_api = jsonObject
									.getJSONObject("search_api");
							JSONArray results = search_api
									.getJSONArray("result");
							if (results.length() > 0) {
								JSONObject result = results.getJSONObject(0);
								JSONArray country = result
										.getJSONArray("country");
								countryCode = country.getJSONObject(0)
										.getString("value");
							}
							System.out.println("countryCode = " + countryCode);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, null);
		locationClient.start();
	}

	public void onTestYoutubeClick(View view) {
		System.out.println("onTestYoutubeClick");

		if (array_user == null) {
			if (!request_user)
				request_user = true;
			httpClient = new RequestHttpClient(url_user, this, this);
			httpClient.start();
		} else {
			openListPage(array_user);
		}

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onRequestStringCallback(String response) {
		// TODO Auto-generated method stub
		if (request_user) {
			request_user = false;
			array_user = new ArrayList<YoutubeObject>();
			try {
				JSONArray jsonArray = new JSONArray(response);

				if (jsonArray != null && jsonArray.length() > 0) {
					for (int i = jsonArray.length() - 1; i >= 0; i--) {
						JSONObject jsonObj = jsonArray.getJSONObject(i);
						YoutubeObject movieObject = new YoutubeObject();
						if (jsonObj.has("title"))
							movieObject.setTitle(jsonObj.getString("title"));
						if (jsonObj.has("desc"))
							movieObject.setDesc(jsonObj.getString("desc"));
						if (jsonObj.has("img"))
							movieObject.setCover(jsonObj.getString("img"));
						if (jsonObj.has("vcode"))
							movieObject.setVcode(jsonObj.getString("vcode"));
						array_user.add(movieObject);
					}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("error = " + e.getMessage());
			}
			openListPage(array_user);
		}
	}

	public void openListPage(ArrayList<YoutubeObject> array) {
		// ImageListActivity.array = array;
		// Intent intent = new Intent(this, ImageListActivity.class);
		// startActivity(intent);
	}
}
