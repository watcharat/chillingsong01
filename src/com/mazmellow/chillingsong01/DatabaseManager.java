package com.mazmellow.chillingsong01;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {

	Context context;

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "chillingsong01.db";

	// Table Name
	private static final String TABLE_BOOKMARK = "bookmarks";

	// -------VERSION 1-------------
	// ---bookmarks---
	private static final String COL_VCODE = "vcode";
	// -------------------------------

	public DatabaseManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.context = context;

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		System.out.println("------ onCreate --------");
		try {
			db.execSQL("CREATE TABLE " + TABLE_BOOKMARK
					+ "("
					// BookmarkID INTEGER PRIMARY KEY,"
					+ " " + COL_VCODE + " TEXT(100));");

			Log.d("CREATE TABLE", "Create Table Successfully.");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("DB ERROR: " + e);
		}

	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		System.out.println("------ onOpen --------");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		System.out.println("------ onUpgrade --------");
		System.out.println("------ oldVersion : " + oldVersion);
		System.out.println("------ newVersion : " + newVersion);

		if (oldVersion == 1 && newVersion == 2) {
//			createTableRead(db);
		}
	}

	// Insert Data
	public boolean insertBookmark(String vcode) {
		// TODO Auto-generated method stub

			try {
				SQLiteDatabase db = this.getWritableDatabase();
				/**
				 * for API 11 and above SQLiteStatement insertCmd; String strSQL
				 * = "INSERT INTO " + TABLE_MEMBER +
				 * "(MemberID,Name,Tel) VALUES (?,?,?)";
				 * 
				 * insertCmd = db.compileStatement(strSQL);
				 * insertCmd.bindString(1, strMemberID); insertCmd.bindString(2,
				 * strName); insertCmd.bindString(3, strTel); return
				 * insertCmd.executeInsert();
				 */

				ContentValues Val = new ContentValues();
				Val.put(COL_VCODE, vcode);

				db.insert(TABLE_BOOKMARK, null, Val);

				if (db != null) {
					db.close();
				}

				return true; // return rows inserted.

			} catch (Exception e) {
				Log.i("insertOrUpdateBookmark", e.getMessage());
				return false;
			}

	}

	public boolean checkHasBookmarkByVcode(String vcode) {
		// TODO Auto-generated method stub
		boolean found = false;
		try {
			// SQLiteDatabase db;
			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_BOOKMARK + " WHERE "
					+ COL_VCODE + " = '" + vcode + "'";
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					found = true;
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			

		} catch (Exception e) {
			Log.i("getBookmarkByTitle", e.getMessage());
		}
		return found;
	}

	public ArrayList<String> getAllBookmark() {
		// TODO Auto-generated method stub

		try {
			ArrayList<String> list = new ArrayList<String>();

			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_BOOKMARK;
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						list.add(cursor.getString(0));
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return list;

		} catch (Exception e) {
			Log.i("getAllBookmark", e.getMessage());
			// System.out.println("ERROR getAllBookmark: "+e);
			return null;
		}

	}

	// Delete Data
	public boolean deleteBookmarkByVcode(String vcode) {
		// TODO Auto-generated method stub

		try {

			SQLiteDatabase db;
			db = this.getWritableDatabase(); // Write Data

			/**
			 * for API 11 and above SQLiteStatement insertCmd; String strSQL =
			 * "DELETE FROM " + TABLE_MEMBER + " WHERE MemberID = ? ";
			 * 
			 * insertCmd = db.compileStatement(strSQL); insertCmd.bindString(1,
			 * strMemberID);
			 * 
			 * return insertCmd.executeUpdateDelete();
			 * 
			 */

			db.delete(TABLE_BOOKMARK, COL_VCODE + " = ?",
					new String[] { String.valueOf(vcode) });

			db.close();
			return true; // return rows delete.

		} catch (Exception e) {
			return false;
		}

	}

}
